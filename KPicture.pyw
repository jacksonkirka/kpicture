'''
KPicture is adapted from the Kivy official demo to be a full application.  It
is based on the official Kivy Example picture demo.
'''

import os
os.environ["KIVY_NO_CONSOLELOG"] = "1"

import main

m = main.KPictureApp()
m.run()