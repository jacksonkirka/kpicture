#!/usr/bin/python3

'''
KPicture is adapted from the Kivy official demo to be a full application.  It
is based on the official Kivy Example picture demo.
'''

import os

# Turn off Console Mode for .pyw files uncomment and change main.py to KPicture.pyw.
# os.environ["KIVY_NO_CONSOLELOG"] = "1"

import sys

# ---------------------------> Install Update Packages -----------------------

is_Android = hasattr(sys, 'getandroidapilevel')
if not is_Android:
    import subprocess
    subprocess.check_call([sys.executable, "-m", "pip", "install", "--upgrade",
        "-r", "requirements.txt"])
    subprocess.check_call([sys.executable, "-m", "pip", "install", "--upgrade",
        "git+https://github.com/pythonic64/hover.git"])

# ---------------------------> End Install Update Packages <------------------

from os.path import join, basename
from random import randint
from glob import glob
from math import sin, cos
from time import time
import pathlib
import certifi
import cups
import win32api
import win32print
from PIL import Image, ImageEnhance
import kivy
kivy.require('2.0.0')
from kivy.app import App
from kivy.factory import Factory
from kivy.logger import Logger
from kivy.uix.scatter import Scatter
from kivy.properties import StringProperty
from kivy.properties import NumericProperty
from kivy.loader import Loader
from kivy.network.urlrequest import UrlRequest
from kivy.clock import Clock
from kivy.cache import Cache
from kivy.storage.jsonstore import JsonStore
from kivy.uix.image import Image as Img
from kivy.utils import platform
from kivy.uix.settings import SettingsWithSpinner
from kivy.config import ConfigParser
from kivy_garden.hover import HoverBehavior, HoverManager, MotionCollideBehavior

__version__ = '0.1' # Version assignment


class Picture(HoverBehavior, Scatter):

    '''Picture is the class that will show the image with a white border and a
    shadow. They are nothing here because almost everything is inside the
    picture.kv. Check the rule named <Picture> inside the file, and you'll see
    how the Picture() is really constructed and used.

    The source property will be the filename to show.
    '''
    # Class Attributes (not instance)
    
    source = StringProperty(None)


# ---------------------------> Touch Events <----------------------------------

    def on_touch_down(self, touch):

        '''
        on_touch_down was adapted from solution by Elliot Garbus.
        '''

        if self.collide_point(*touch.pos):
            #self.time = time()
            app = App.get_running_app()
            app.selected_pic = self  # self is the selected widget
            if app.picsaved:
                app.picsaved.opacity = app.picopacity
            self.opacity = app.selectedpicopacity
            app.picsaved = self
            app.fname_pic = self.source  # Pass filename path to App
            app.root.ids.displayfilename.text = basename(app.fname_pic)
            app.get_pic_info(app.fname_pic)
            if touch.is_double_tap:
                app.zoom_pic_large()
            if touch.is_triple_tap:
                app.drop_pic()
            if touch.is_mouse_scrolling:
                if touch.button == 'scrolldown':
                    self.scale = self.scale + .5
                elif touch.button == 'scrollup' and self.scale > 1.2:
                    self.scale = self.scale - .5
        return super().on_touch_down(touch)

    def on_hover_enter(self, me):

        '''
        on_hover_enter changes pictures opacity to 1.
        '''

        self.opacity = 1

    def on_hover_leave(self, me):
        '''
        on_hover_leave returns picture opacity to settings value.
        '''

        app = App.get_running_app()
        self.opacity = app.picopacity
        if app.micropop:
            app.root.remove_widget(app.micropop)
            app.micropop = None

    def on_touch_up(self, touch):

        '''
        on_touch_up detects a touch up event then displays the MicroContextPop.
        '''
        app = App.get_running_app()
        if app.micropop == None and self.collide_point(*touch.pos):
            app.micropop = Factory.MicroContextPop(pos=touch.pos)
            app.root.add_widget(app.micropop)
        return super().on_touch_up(touch)

# -------------------------> End Touch Events <--------------------------------

class KPictureSettings(SettingsWithSpinner):

    """
    KPictureSettings is the settings class for which provides a settings gui
    for the app.
    """

    def on_close(self):

        '''
        on_close is an event triggered by clicking the settings
        close button.  This runs Logger as below.
        '''
        Logger.info("main.py: KPictureSettings.on_close")

    def on_config_change(self, config, section, key, value):
        '''
        on_config-change is an event triggered when the user
        navigates to the settings panels and makes an change
        to any any values in the panel.  Note that
        on_config_change is both a method in KPictureSettings
        class and KPictureApp class.  Logger is also run as
        below on this settings method.
        '''
        Logger.info(
            "main.py: KPictureSettings.on_config_change: "
            "{0}, {1}, {2}, {3}".format(config, section, key, value))


class KPictureApp(App):

    '''
    __init__ provided by Elliot Garbus.  Sets up self.selected_pic.
    edited and upgraded by Kirk A Jackson to provide additional attributes
    and settings save to JSON files.
    '''
    
    # Class Attributes (not instance) background had to be defined as class
    # StringProperty per Elliot Garbus
    
    background = StringProperty(None)
    settingsfilebase = StringProperty('')
    picopacity = NumericProperty(None)
    selectedpicopacity = NumericProperty(None)



    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.hover_manager = HoverManager()

        # create the instance variable
        self.selected_pic = None  
        self.img_saved = None
        self.slide_idx = 0
        self.img_idx = 0
        self._x = 0
        self._y = 0
        self.fname_pic = ''
        self.text = ''
        self.bin = None
        self.picsaved = None
        self.filename_list = []
        self._directories_idx = 0
        self.micropop = None
        self._directories = []
        self.kpicture_message = ''
        self._settings = []
        self.settingsfile = None
        self._settings_idx = 0
        self.slide_list = []
        self.reduce_idx = 0
        self._photo_info = ''
        self._slide = None
        self.browse_event = None
        self.settingsfile = None
        self.slide_event = None
        self._reduce_event = None
        self.cache_list = []
        self.cache_restored = False
        self._state_json = JsonStore('state.json')
        Cache.register('picture_cache', limit=36, timeout=3600)
        Cache.register('browse_cache', limit=180, timeout=3600)
        self.config = ConfigParser()
        self.usrhome = str(pathlib.Path.home())
        self.settingsfile = self._state_json.get('lastconfig')['configfile']
        self.settingsfilebase = basename(self.settingsfile)
        # Note for android loading last settins file used needs work.
        if platform == 'android' and os.path.exists('../.kpicture.ini'):
            self.config.read('../.kpicture.ini')
        else:
            self.config.read(self.settingsfile)
            self._settings.append(self.settingsfile)
        self.update_getters()
# ----------------------> Register Cache <-------------------------------------

        Cache.register('browse_cache', limit=self._cachebrowsenum,
                      timeout=self._cachebrowsetime)       
        Cache.register('picture_cache', limit=self._cachepicnum,
                      timeout= self._cachepictime)
        
# ----------------------> End Register Cache <---------------------------------
# --------------------------> Program State <----------------------------------

        self._directories = self._state_json.get('dir')['pdirectories']
        if self._state_json.get('stgs')['psettings']:
            self._settings = self._state_json.get('stgs')['psettings']
        self.cache_list = self._state_json.get('piccache')['cache']
        self.curdir = self._state_json.get('lastdir')['curdir']
        if self.curdir == '/' and platform == 'android':
            self.curdir = '/sdcard/Pictures'

# -------------------------> End Program State <------------------------------- 
# -----------------------> Getters <--------------------------------------------

    def update_getters(self):
        '''
        update_getters updates the applications environment variables in membory
        and is callable from loadable configuration files.
        '''

        self.img_size = self.config.getint('Image Settings', 'imgsize')
        self.picborder = self.config.getint('Image Settings', 'imgborder')
        self.picopacity = self.config.getfloat('Image Settings', 'imgopacity')
        self.selectedpicopacity = self.config.getfloat('Image Settings',
                 'selectedimgopacity')
        self.reduce_intrvl = self.config.getint('Image Settings',
                'imgredsizeintrvl')
        self.reduce_prct = self.config.getfloat('Image Settings',
                'imgredsizeprcnt')
        self._enh_factor = self.config.getfloat('Image Settings', 'enhance')
        self.img_browse_limit = self.config.getint('Image Settings',
                'browselmt')
        self.zoom = self.config.getfloat('Image Settings', 'zoom')
        self._cachebrowsenum = self.config.getint('Cache Settings',
                'cachebrowsenum')
        self._cachebrowsetime = self.config.getint('Cache Settings',
                 'cachebrowsetime')
        self._cachepicnum = self.config.getint('Cache Settings', 'cachepicnum')
        self._cachepictime = self.config.getint('Cache Settings',
                 'cachepictime')
        self.bar_pos = self.config.get('Interface Settings','actbarpos')
        self.background = self.config.get('Interface Settings',
                'background')
        self.slide_speed = self.config.getint('Slide Show Settings',
                'slide_duration')
        Loader.max_upload_per_frame = self.config.getint('Thread Settings',
                'maxup')
        Loader.num_workers = self.config.getint('Thread Settings', 'numwrks')
        self.img_batch = self.img_browse_limit
        self.rock_left = self.config.getfloat('Image Settings', 'rockleft')
        self.rock_right = self.config.getfloat('Image Settings', 'rockright')
        self.img_centered_on_canvas = self.config.getboolean('Image Settings',
                'imgcentered')
        self.x_inc = self.config.getint('Image Settings', 'xinc')
        self.y_inc = self.config.getint('Image Settings', 'yinc')
        self.imgs_per_row = self.config.getint('Image Settings', 'imgperrow')
        self.y_rows_inc = self.config.getint('Image Settings', 'yrowinc')
        self.img_circle = self.config.getboolean('Image Settings', 'imgcircle')
        self.img_circle_radius = self.config.getint('Image Settings',
                'imgcircler')
        self.img_circle_degrees = self.config.getint('Image Settings',
                'imgcircled')
        self.img_custom = self.config.getboolean('Custom Settings',
                'imgcustom')
        self.x_eq = self.config.get('Custom Settings', 'customx')
        self.y_eq = self.config.get('Custom Settings', 'customy')

# ----------------------> End Getters <----------------------------------------

# --------------------> Application Startup and Restore <----------------------
    def on_start(self):
 
        '''
        on_start runs restore_cache on startup.
        '''
        super().on_start()
        self.root_window.register_event_manager(self.hover_manager)
        self.restore_cache(self.cache_list)
        self.display_cache()
        self.switch_settings()

# -----------------> End Application Startup and Restore <---------------------
# ------------------------>End Class Attributes <------------------------------      
        
    def build(self):

        '''
        build sets up the GUI interface specified in kpicture.kv.
        '''

        self.settings_cls = KPictureSettings
        self.icon = 'image/visualsoft.png'
        
# -----------------------> Settings <-------------------------------------------

    def build_config(self, config):

        """
        Set the default values for the configs sections.
        """

        config.setdefaults('Image Settings', {'imgsize': 400, 'imgborder': 36,
            'imgopacity': .7, 'selected_imgopacity': 1.0,
            'imgredsizeintrvl': 3,'imgredsizeprcnt': 0.25,'enhance': 1.055,
                'browselmt': 36, 'zoom': 3.0, 'rockleft': 30, 'rockright': 30,
                'imgcentered': False, 'xinc': 100, 'yinc': 100,
                'imgperrow': 36, 'yrowinc': 400, 'imgcircle': False,
                'imgcircler': 0, 'imgcircled': 0})
        config.setdefaults('Cache Settings', {'cachebrowsenum': 180,
                'cachebrowsetime': 3600, 'cachepicnum': 36, 'cachepictime': 3600})
        config.setdefaults('Interface Settings', {'actbarpos': 'bottom',
                'background':''})
        config.setdefaults('Slide Show Settings', {'slide_duration': 5})
        config.setdefaults('Thread Settings', {'maxup': 2, 'numwrks': 2})
        config.setdefaults('Custom Settings', {'imgcustom': False,
                'customx': 'self.x = 0', 'customy': 'self.y = 0'})

    def build_settings(self, settings):

        """
        Add our custom section to the default configuration object.
        """

        settings.add_json_panel('Image Settings', self.config, 'image.json')
        settings.add_json_panel('Cache_Settings', self.config, 'cache.json')
        settings.add_json_panel('Interface Settings', self.config,
                'interface.json')
        settings.add_json_panel('Slide Show Settings', self.config,
                'slideshow.json')
        settings.add_json_panel('Thread Settings', self.config, 'thread.json')
        settings.add_json_panel('Custom Settings', self.config, 'custom.json')

    def on_config_change(self, config, section, key, value):

        """
        Respond to changes in the configuration.  Note: you can call other
        methods in addition to settings update.
        """

        Logger.info("main.py: App.on_config_change: {0}, {1}, {2}, {3}".format(
            config, section, key, value))
        _circle_keys = ('imgcircle', 'imgcircler', 'imgcircled')
        if key in _circle_keys:
            self.config_change_circle(key, value)
        if section == 'Image Settings':
            self.config_change_image(key, value)
        if section == 'Cache Settings':
            self.config_change_cache(key, value)
        if section == 'Interface Settings':
            self.config_change_interface(key, value)
        if section == 'Slide Show Settings':
            self.config_change_slide_show(key, value)
        if section == 'Thread Settings':
            self.config_change_thread(key, value)
        if section == 'Custom Settings':
            self.config_change_custom(key, value)

    def config_change_image(self, key, value):

        '''
        config_change_image changes the value of settings in the image
        interface.
        '''

        if key == 'imgsize':
            self.img_size = int(value)
        elif key == 'imgborder':
            self.picborder = int(value)
        elif key == 'imgopacity':
            self.picopacity = float(value)
        elif key == 'selectedimgopacity':
            self.selectedpicopacity = float(value)
        elif key == 'imgredsizeintrvl':
            self.reduce_intrvl = int(value)
        elif key == 'imgredsizeprcnt':
            self.reduce_prct = float(value)
        elif key == 'enhance':
            self._enh_factor = float(value)
        elif key == 'browselmt':
            self.img_browse_limit = int(value)
        elif key == 'zoom':
            self.zoom = float(value)
        elif key == 'rockleft':
            self.rock_left = float(value)
        elif key == 'rockright':
            self.rock_right = float(value)
        elif key == 'imgcentered':
            self.img_centered_on_canvas = bool(int(value))
        elif key == 'xinc':
            self.x_inc = int(value)
        elif key == 'yinc':
            self.y_inc = int(value)
        elif key == 'imgperrow':
            self.imgs_per_row = int(value)
        elif key == 'yrowinc':
            self.y_rows_inc = int(value)

    def config_change_cache(self, key, value):

        '''
        config_change_cache changes the values for caches
        browse and picture cache when edited.
        '''
        if key == 'cachebrowsenum':
            self._cachebrowsenum = int(value)
        if key == 'cachebrowsetime':
            self._cachebrowsetime = int(value)
        if key == 'cachepicnum':
            self._cachepicnum = int(value)
        if key == 'cachepictime':
            self._cachepictime = int(value)

    def config_change_circle(self, key, value):

        '''
        config_change_circle changes the values entered in the image
        settings dialogue for circle layout of photos.
        '''

        if key == 'imgcircle':
            self.img_circle = bool(int(value))
        elif key == 'imgcircler':
            self.img_circle_radius = int(value)
        elif key == 'imgcircled':
            self.img_circle_degrees = int(value)

    def config_change_interface(self, key, value):

        '''
        config_change_interface changes the values entered in the interface
        settings panel.
        '''

        if key == 'actbarpos':
            self.root.remove_widget(self.root.ids.menubar)
            self.bar_pos = value
            self.root.add_widget(self.root.ids.menubar)
        elif key == 'background':
            self.background = value

    def config_change_slide_show(self, key, value):

        '''
        config_change_slideshow changes values entered in the slide show
        dialogue panel.
        '''

        if key == 'slide_duration':
            self.slide_speed = int(value)

    def config_change_thread(self, key, value):

        '''
        config_change_thread changes the values entered in he thread
        dialogue panel.
        '''

        if key == 'maxup':
            Loader.max_upload_per_frame = int(value)
        elif key == 'numwrks':
            Loader.num_workers = int(value)

    def config_change_custom(self, key, value):

        '''
        config_change_custom changes the values entered in he custom
        dialogue panel.
        '''

        if key == 'imgcustom':
            self.img_custom = bool(int(value))
        elif key == 'customx':
            self.x_eq = value
        elif key == 'customy':
            self.y_eq = value
            
    def save_settings_config_file(self, _config_file):
        ''''
        save_settings_config_file saves the full path name of the
        current settings.ini file to state.json.
        '''
        self._state_json.put('lastconfig', configfile=_config_file)    
    
    def save_settings_list(self, _stgs):

        '''
        save_json saves the list of settings files in
        self._setings to stgs.json.
        '''

        self._state_json.put('stgs', psettings=_stgs)

# -------------------------------> Load/Save Settings <-----------------------

    def switch_settings(self):

        '''
        switch_settings switches between multiple settings files in the list
        stored in self._settings.
        '''

        if self._settings:
            if self._settings_idx == len(self._settings):
                self._settings_idx = 0
            self.settingsfile = self._settings[self._settings_idx]
            self.settingsfilebase = basename(self.settingsfile)
            self.config.read(self._settings[self._settings_idx])
            self.update_getters()
            
            # Purge and restore the cache
            self.purge_cache('browse_cache')
            Cache.register('browse_cache', self._cachebrowsenum,
                self._cachebrowsetime)
            self.purge_cache('picture_cache')
            self.cache_restored = False
            self.restore_cache(self.cache_list)

            self._settings_idx += 1

    def load_settings(self, _settings_file):

        '''
        load_settings takes path passed in and loads into memory the
        settings defined in the config file.
        '''
        self.settingsfile = _settings_file
        self.settingsfilebase = basename(self.settingsfile)
        self._settings.append(self.settingsfile)
        self.config.read(_settings_file)
        self.update_getters()
        
    def open_settings(self,*args):
        '''
        open_settings from stackoverflow very good
        because it only updates when you look at
        settings.
        '''

        self.destroy_settings() # clear settings
        return super().open_settings(*args)

    def save_settings(self):

        '''
        save_settings_to_file saves settings to the last file read.
        '''
        self.config.write()
        self.save_settings_config_file(self.settingsfile)

# -------------------------------> End Load/Save Settings <-------------------

    def close_settings(self, settings=None):

        """
        The settings panel has been closed.
        """

        Logger.info("main.py: App.close_settings: {0}".format(settings))
        super().close_settings(settings)

# -----------------------> End Settings <--------------------------------------
# -----------------------> File to Variale <-----------------------------

    def text_file_to_var(self, _textfile):

        '''
        text_file_to_var accepts a file.txt and returns the text in the file.
        '''
        try:
            
            with open(_textfile, 'rt') as f:
                self._text = f.read()

        except Exception as e: 
                _err = str(e)
                self.kpicture_message = 'An error occurred: '+_err
                Factory.Message().open()
                return
 
    def binary_file_to_var(self, _binfile):

        '''
        binary_file_to_var accepts a binary file and returns the
        contents of the file.
        '''
        try:
            with open(_binfile, 'rb') as f:
                self._bin = f.read()
        except Exception as e: 
                _err = str(e)
                self.kpicture_message = 'An error occurred: '+_err
                Factory.Message().open()
                return
 

    
# ------------------------> End File to Variable <------------------------
# ------------------------> About <--------------------------------------------

    def show_about(self):
        '''
        about dispalys the text from the about file.
        '''

        self.text_file_to_var('about.txt')
        Factory.About().open()

# ------------------------> End About <----------------------------------------
# ---------------------------> Printing Services <-----------------------------


    def print_kpicture_image(self, _picfilename):

        '''
        print_kpicture_image prints image passed in 
        and by branching based on the OS detected.
        '''
        # Detect operation system
        if sys.platform.startswith('linux') or sys.platform == 'darwin':

            #Branch to Unix if darwin
            self.print_image_unix(_picfilename)

        elif sys.platform == 'win32':

            #Brach to Windows if win32
            self.print_kpicture_image_windows(_picfilename)

        else:
            # If OS is not darwin or win32  message to user not supported
            plt = sys.platform 
            _msg = 'The platform '+plt+' is not supported for printing services.'
            self.kpicture_message = _msg
            Factory.Message().open()
            return
        
    def print_kpicture_cache(self, _cachelist):

        '''
        print_kpicture_image prints images passed in to default printer
        by branching based on the OS detected.
        '''
        # if OS is darwin branch to Unix
        if sys.platform.startswith('linux') or sys.platform == 'darwin':

            for _cache_pic in self.cache_list:

                self.print_cache_images_unix(_cache_pic)
            return

        # if OS is win32 branch to Windows
        elif sys.platform == 'win32':

            # if cache_list then print all pics in cache list
            if self.cache_list:

                for _cache_pic in self.cache_list:

                    self.print_kpicture_cache_image_windows(_cache_pic[0])

                # Send message to user print job complete.
                printer_name = win32print.GetDefaultPrinter()
                self.kpicture_message = \
                "Print job was completed for pictures in the cache on "+printer_name+"." 
                Factory.Message().open()
            
        else:

            # If OS is not darwin or win32 message user not supported
            plt = sys.platform 
            _msg = 'The platform '+plt+' is not supported for printing services.'
            self.kpicture_message = _msg
            Factory.Message().open()
            return


    
    def print_kpicture_cache_image_windows(self, _picpath):

        '''
        print_kpicture_cache_image_windows prints file passed in to
        the Microsoft Windows default printer.  Solution provided
        in part by Chat GPT AI and Kirk A Jackson.
        '''
        
        try:
            # Get default printer name
            printer_name = win32print.GetDefaultPrinter()

            # Create printer handle object passing in printer name
            printer_handle = win32print.OpenPrinter(printer_name)

            # Notify print spooler and get job id.
            win32print.StartDocPrinter(
                printer_handle, 1, ('Print Job', None, 'RAW')) 

            # read file as binary 
            with open(_picpath, 'rb') as f:
                data = f.read()

                # Copy read bytes to printer
                win32print.WritePrinter(printer_handle, data)

            # End of print job
            win32print.EndDocPrinter(printer_handle)

            # Close printer
            win32print.ClosePrinter(printer_handle)



        except Exception as e:

            # Notify user that an error has occurred.
            _err = str(e)
            self.kpicture_message = 'An error occurred while printing: '+ _err
            Factory.Message().open()

    def print_kpicture_image_windows(self, _picpath):

        '''
        print_kpicture_image_windows prints file passed in to
        the Microsoft Windows default printer.  Solution provided
        in part by Chat GPT AI and Kirk A Jackson.
        '''
        printer = win32print.GetDefaultPrinter()
        try:

            win32api.ShellExecute(0, "print",_picpath, printer, ".", 0)

        except Exception as e:

            # Notify user that an error has occurred.
            _err = str(e)
            self.kpicture_message = 'An error occurred while printing: '+ _err
            Factory.Message().open()


    def print_kpicture_image_unix(self, _picpath):

        '''
        print_kpicture_image_unix prints file passed in to
        the Common Unix Printing System.
        '''

        conn = cups.Connection()
        #printer_name = 'your_printer_name'
        conn.printFile(printer_name, _picpath, 'Print Job', {})

# -----------------------> End Printing Services <-----------------------------

# ----------------------> Cache Management <-----------------------------------

    def add_pic_to_cache(self, key, _pic):

        '''
        add_pic_to_cache adds a picture to the cache with assigned key value
        and object passed in if it has not already been added.
        '''

        if not self.cache_list:
            self.append_cache(key, _pic)
            return
        else:
            for _picture in self.cache_list:
                if _picture[0] == key:
                    self.kpicture_message = 'This picture is already in the cache.'
                    Factory.Message().open()
                    return
            self.append_cache(key, _pic)
        
    def append_cache(self, key, _pic):

        '''
        append_cache appends the cache with
        an additional picture.
        '''

        Cache.append('picture_cache', key, _pic)
        pic_in_cache = [key, _pic.pos, _pic.rotation, _pic.scale]
        self.cache_list.append(pic_in_cache)
        self.save_cache(self.cache_list)


    def get_pic_from_cache(self, key):

        '''
        get_pic_from_cache retrieves picture previously added to the cache
        by key passed in.
        '''

        pic_from_cache = Cache.get('picture_cache', key)
        return pic_from_cache

    def remove_pic_from_cache(self, key):

        '''
        reomve_pic_from_cache removes a picture from the cash based on key
        value passed in.
        '''

        Cache.remove('picture_cache', key)
        for _idx in self.cache_list:
            if key == _idx[0]:
                self.cache_list.remove(_idx)
        self.clear_pics()
        self.display_cache()
        self.save_cache(self.cache_list)

    def purge_cache(self, _category):

        '''
        purge_cache purges the category of cache passed.
        '''
        Cache.remove(_category)

    def restore_cache(self, _cache):

        '''
        restore_cache restores the cache from self.cache_list 
        '''

        if self.cache_list and not self.cache_restored:
            for key in self.cache_list:
                picture = Picture(source=key[0], pos=key[1], rotation=key[2],
                         scale=key[3])
                Cache.append('picture_cache', key[0], picture)
        self.cache_restored = True

    def clear_cache(self):

        '''
        clear_cache clears the cache and the current cache_list then displays
        and empty cache display.
        '''

        for _pic in self.cache_list:
            Cache.remove('picture_cache', _pic[0])
        self.cache_list = []
        self.clear_pics()
        self.display_cache()
        self.save_cache(self.cache_list)

    def display_cache(self):

        '''
        display_cache displays images saved to the cache in memory.  It is
        very quick.
        '''

        if self.cache_restored:
            self.clear_pics()
            for key in self.cache_list:
                self.add_pic_to_canvas_from_cache(key[0],key[1], key[2],key[3])

    def add_pic_to_canvas_from_cache(self, key, _pos, _rot, _scale):

        '''
        add_pic_to_canvas_from_cache adds a picture to the canvas that has
        been added to the cache with filename as key.
        '''

        cache_pic = self.get_pic_from_cache(key)
        cache_pic.pos = _pos
        cache_pic.rotation = _rot
        cache_pic.scale = _scale
        self.root.add_widget(cache_pic)

    def save_cache(self, _cache):

        '''
        save_cache saves updates and saves self.cache_list to cache.json.
        '''

        if self.cache_list:
            _update = []
            for key in self.cache_list:
                _pic = self.get_pic_from_cache(key[0])
                cache_pic_update = [key[0], _pic.pos, _pic.rotation, _pic.scale]
                _update.append(cache_pic_update)
            self.cache_list = _update
        self._state_json.put('piccache', cache=self.cache_list)

# ----------------------> End Cache Management <--------------------------------

# ----------------------> Local Access and Navigation <-------------------------

    def get_pictures(self):

        '''
        get_pictures edited by Kirk Jackson and adapted from
        from Kivy demo.   rotation=randint(-30, 30)
        '''

        # get any files into images directory
        self.clear_pics()
        _img = 0
        y_saved = 0
        if not self.img_circle:
            self._y = 40  # padding for menu
        _total_degrees = 1
        self.img_size = int(self.config.get('Image Settings', 'imgsize'))
        _center_of_picture = self.img_size/2
        _circle_center = self.root.center
        for pic in self.filename_list:
            try:
                if self.img_custom:
                    self.img_centered_on_canvas = False
                    self.compute_x_y(self.x_eq, self.y_eq)
                    self.add_pic_to_canvas(
                        self.filename_list[self.img_idx])
                    self.img_idx += 1
                    if self.img_idx == len(self.filename_list):
                        return
                    if self.img_idx == self.img_batch:
                        self._x = 0
                        self._y = 0
                        return
                if self.img_circle:
                    self.img_centered_on_canvas = False
                    self._x = _circle_center[0] + self.img_circle_radius * (
                            cos(_total_degrees)) - _center_of_picture
                    self._y =  _circle_center[1] + self.img_circle_radius * (
                            sin(_total_degrees)) - _center_of_picture
                    self.add_pic_to_canvas(
                        self.filename_list[self.img_idx])
                    _total_degrees += self.img_circle_degrees
                    self.img_idx += 1
                    if self.img_idx == len(self.filename_list):
                        return
                    if self.img_idx == self.img_batch:
                        self._x = 0
                        self._y = 0
                        return
                if not self.img_circle and not self.img_custom:
                    # Note: self.img_centered_on_canvas is in kpicture.kv
                    self.add_pic_to_canvas(
                        self.filename_list[self.img_idx])
                    self.img_idx += 1
                    if self.img_idx == len(self.filename_list):
                        return
                    if not self.img_centered_on_canvas:
                        self._x += self.x_inc
                        self._y += self.y_inc
                    _img += 1
                    if _img >= self.imgs_per_row:
                        y_saved += self.y_rows_inc
                        self._y = y_saved
                        self._x = 0
                        _img = 0
                    if self.img_idx >= self.img_batch:
                        self._x = 0
                        self._y = 0
                        return
            except Exception as e:
                Logger.exception('Pictures: Unable to load <%s>' % pic)

    def compute_x_y(self, x_eq, y_eq):

        '''
        compute_x_y updates the value of self._x and self._y based on
        equation provided by user in settings.  User equation must be
        set to 'self._x = equation' and 'self._y = equation' respectively.
        There is not limit to layout.
        '''

        exec(x_eq)
        exec(y_eq)

    def add_pic_to_canvas(self, _filename=''):

        '''
        add_pic_to_canvas adds pic to FloatLayout Canvas.
        '''
        picture = Cache.get('browser_cache', _filename)
        if picture is None:
            picture = Picture(
            source=_filename,
            rotation=randint(-self.rock_left, self.rock_right))
            Cache.append('browse_cache', _filename, picture)
        self.root.add_widget(picture)

    def get_pic_filenames(self):

        '''
        get_pic_filenames fills the self.filename_list with the
        filenames of the pictures in the directory.
        '''

        for filename in glob(join(self.curdir, '*')):
            if filename.endswith(('.jpg', 'jpeg', '.png', '.gif')):
                self.filename_list.append(filename)
        self._directories.append(self.filename_list)
 
    def switch_dir(self):

        '''
        switch_dir switches between multiple directory file lists stored in
        self._directories.
        '''

        if self._directories:
            if self._directories_idx == len(self._directories):
                self._directories_idx = 0
            self.filename_list = self._directories[self._directories_idx]
            self.curdir = os.path.dirname(self.filename_list[0])
            self._directories_idx += 1
            self.img_idx = 0
            self._x = 0
            self._y = 0
            self.img_batch = self.img_browse_limit
            self.get_pictures()

    def get_pics_from_url(self, url):

        '''
        get _pics from url is in development to get pictures via api from the
        internet.
        '''

        self.clear_pics()
        self.filename_list = []
        self.img_idx = 0
        # for filename in glob(join(url, '*.jpg')):
        #    self.filename_list.append(filename)
        # print(self.filename_list)
        # self.get_pictures()

    def prev(self):

        '''
        prev retreats to previous batch of pictures defined in
        self.img_browse_limit.
        '''

        if self.img_idx > self.img_browse_limit:
            self.img_batch = self.img_batch - self.img_browse_limit
            self.img_idx = self.img_idx - 2 * self.img_browse_limit
            self._x = 0
            self._y = 0
            self.clear_pics()
            self.get_pictures()
        else:
            _no_pics = len(self.filename_list)
            _no_batches = int(_no_pics/self.img_browse_limit)
            _remainder = _no_pics % self.img_browse_limit
            if _remainder:
                self.img_batch = self.img_browse_limit + (
                       _no_batches * self.img_browse_limit)
                self.img_idx = _no_batches * self.img_browse_limit
            else:
                self.img_batch = _no_batches * self.img_browse_limit
                self.img_idx = self.img_batch - self.img_browse_limit
            self._x = 0
            self._y = 0
            self.clear_pics()
            self.get_pictures()

    def next(self):

        '''
        next advances to the next batch defined by self.img_browse_limit
        '''

        if self.img_idx == self.img_batch:
            self.img_batch = self.img_idx + self.img_browse_limit
            self.get_pictures()
        else:
            self.img_idx = 0
            self._x = 0
            self._y = 0
            self.img_batch = self.img_browse_limit
            self.clear_pics()
            self.get_pictures()

    def select(self, directory):

        '''
        select provided by Kirk A Jackson.
        '''

        self.clear_pics()
        self.filename_list = []
        self.img_idx = 0
        self._x = 0
        self._y = 0
        self.curdir = directory
        self.get_pic_filenames()
        # self.get_pictures()

    def clear_pics(self):

        '''
        Clear pictures provided by Kirk Jackson.
        '''

        if len(self.root.children) > 1:
            self.root.clear_widgets(self.root.children[:-1])


    def drop_pic(self):

        '''
        dop_pic provided by Elliot Garbus.
        '''

        if self.selected_pic:
            self.root.remove_widget(self.selected_pic)

    def zoom_pic_large(self):

        '''
        zoom_pic_large zooms the selected pic to large size.  Double tapping
        same object a second time scales to 1.
        '''

        if self.selected_pic == self.img_saved:
            self.selected_pic.scale = 1
            self.img_saved = None
        else:
            self.selected_pic.scale = self.zoom
            self.img_saved = self.selected_pic

# ----------------------> End Local Access and Navigation <--------------------
# ----------------------> Picture Effects and Sizes <--------------------------

    def pic_to_black_white(self):

        '''
        pic_to_black_white accepts a color multiwidget child then
        creates a 'bw' directory then returns a  b/w multiwidget child
        to the canvas. It does not edit the original file.
        '''

        if self.fname_pic:                                     # Color path?
            if os.path.splitext(self.fname_pic)[1] == '.gif':  # gif then ret
                return
            _filename = os.path.basename(self.fname_pic)       # get filename
            _curdir = os.path.dirname(self.fname_pic)          # get dir
            if not os.path.isdir(_curdir+'/bw'):               # subdir exist?
                os.mkdir(_curdir+'/bw')                        # No, create it
            _file = _curdir+'/bw/'+_filename                   # create bw path
            if not os.path.isfile(_file):                      # bwfile exist?
                _img = Image.open(self.fname_pic)              # No, create it
                _bw = _img.convert('L') 
                _bw.save(_file)
            _converted_bw  = Picture(source = _file,
                    rotation=randint(self.rock_left,
                        self.rock_right))                      # Create Picture
            self.root.add_widget(_converted_bw)                # Add to canvas
       
    def enhance_pic(self):

        '''
        enhance_pic accepts a multiwidget child then creates a 'enh' directory
        then returns a enhanced  multiwidget child to the canvas. It does not
        edit the original file.
        '''

        if self.fname_pic:                                     # Current path?
            if os.path.splitext(self.fname_pic)[1] == '.gif':  # gif then ret
                return
            _filename = os.path.basename(self.fname_pic)       # extract file
            _curdir = os.path.dirname(self.fname_pic)          # get dir
            if not os.path.isdir(_curdir+'/enh'):              # subdir exist?
                os.mkdir(_curdir+'/enh')                       # No, create it
            _file = _curdir+'/enh/'+_filename                  # make enh path
            if not os.path.isfile(_file):                      # enhfile exist?
                _img = Image.open(self.fname_pic)              # No, create it
                _bri = ImageEnhance.Brightness(_img).enhance(
                        self._enh_factor)                      # Bright
                _sat = ImageEnhance.Color(_bri).enhance(
                        self._enh_factor)                      # Color
                _shp = ImageEnhance.Sharpness(_sat).enhance(
                        self._enh_factor)                      # Sharpness
                _fin = ImageEnhance.Contrast(_shp).enhance(
                        self._enh_factor)                      # Contrast
                _fin.save(_file)                               # Save file
            _enhanced  = Picture(source=_file,
                    rotation=randint(self.rock_left,
                        self.rock_right))                      # Picture
            self.root.add_widget(_enhanced)                    # Add to canvas

    def schedule_reduce_pics(self, _pic_list):

        '''
        schedule_reduce_pics reduce_pics in a thread so it runs
        in the background without locking up the app.
        '''
        self.reduce_list = _pic_list
        self._directories.pop() # Needed to remove dir with large files
        if len(self.reduce_list) > 0:
            self.reduce_idx = 0
            self._reduce_event = Clock.schedule_interval(
                    self.reduce_get_pic_path, self.reduce_intrvl)
    
    def reduce_get_pic_path(self, delay_time):
        
        '''
        reduce_get_pic_path gets the path of the pic to reduce
        based on the value in self.reduce_idx.
        '''
        if not self.reduce_idx > len(self.reduce_list)-1:
            self.reduce_pic_size(self.reduce_list[self.reduce_idx])
            self.reduce_idx += 1
        else:
            Clock.unschedule(self._reduce_event)

    def reduce_pics(self, _pic_list):
        
        '''
        reduce_pics reduces all pictures in a directory to smaller size.
        '''

        for pic in _pic_list:
            self.reduce_pic_size(pic)


    def reduce_pic_size(self, _picture):

        '''
        reduce_pic_size takes a picture and reduces its size in pixels to
        fit and respond to mobile devices but maintain quality.
        '''
        _filename = os.path.basename(_picture)                 # extract file
        _curdir = os.path.dirname(_picture)                    # get dir
        if not os.path.isdir(_curdir+'/resized'):              # subdir exist?
            os.mkdir(_curdir+'/resized')                       # No, Create it
        _file = _curdir+'/resized/'+_filename                  # make rsz path
        _img = Image.open(_picture)                            # Open file
        _img_resized = _img.resize(
                (round(_img.size[0]*self.reduce_prct),          # Resize it
                round(_img.size[1]*self.reduce_prct)))
        _img_resized.save(_file)                               # Save resized

# ----------------------> End Picture Effects and Sizes <----------------------

# ----------------------> Misc Methods <---------------------------------------

    def on_pause(self):

        '''
        on_pause was included in the original demo but unsure of its purpose.
        '''

        return True

# ---------------------->End Misc Methods <-------------------------------------

# -------------------> Methods needed for internet request <--------------------

    def synchronous_api_request(self, url):

        '''
        Method synchronous_api_request is generic synchronoous request. Note
        that returns should be JSON.
        '''
        try:

            req = UrlRequest(url, ca_file=certifi.where())  # Restful API Request
            req.wait(delay=.1)  # synchronous request
            return req.result  # return data to caller

        except Exception as e:
            # Notify user that an error has occurred.
            _err = str(e)
            self.kpicture_message = 'An error occured during request: '+ _err
            Factory.Message().open()


    def asynchronous_api_request(self, url):

        '''
        Method asynchronous_api_request is coded using Kivy urlrequest.py.
        Note that returns should be JSON.
        '''
        try:

            Clock.start_clock()         # Start the Kivy clock
            req = UrlRequest(url)       # get request thread
            while not req.is_finished:  # start while loop to test is_finished
                Clock.tick()            # tick clock per cycle
            Clock.stop_clock()          # Stop the clock
            return req.result           # Return the results

        except Exception as e:

            # Notify user that an error has occurred.
            _err = str(e)
            self.kpicture_message = 'An error occured during request: '+ _err
            Factory.Message().open()

# -------------------> End Methods need for internet request <------------------


# ------------------------------> Slide Shows-----------------------------------

    def slide_show(self):

        '''
        scatter_slide_show starts a slide show for pictures in
        self.filename_list.
        '''

        self.clear_pics()
        self.slide_list = self.filename_list.copy()
        if len(self.filename_list) > 0:
            self.img_size = self.root.size
            self.slide_idx = 0
            self.slide_event = Clock.schedule_interval(self.show_slide,
                    self.slide_speed)

    def show_slide(self, delay_time):

        '''
        show slide displays current slide by index in the list. Note pylint
        caught 'dt' above as not used but it is delay time defined in settings.
        '''

        if self._slide is not None:

            self.root.remove_widget(self._slide)
        self._slide = Img(
                    source=self.slide_list[self.slide_idx],
                    size=self.root.size, keep_ratio=True,
                    allow_stretch=True,
                    size_hint=(None, None))
        self.root.add_widget(self._slide)
        Logger.info("main.py: App.show_slide: {:.10f} seconds.".format(
                delay_time))
        self.slide_idx += 1
        if self.slide_idx == len(self.slide_list):
            self.stop_slide_show()

    def last_slide(self, delaytime):

        '''
        last slide simply removes the last slide in the self.slide_list.
        '''

        self.root.remove_widget(self._slide)
        self.img_size = int(self.config.get('Image Settings', 'imgsize'))


    def prev_slide(self):

        '''
        prev_slide displays the previous slide in self.slide_list.
        '''

        if self.slide_list:
            if self._slide is not None:
                self.root.remove_widget(self._slide)
            self.slide_idx -= 1
            if self.slide_idx < 0:
                self.slide_idx = len(self.slide_list) - 1
            self._slide = Img(
                    source=self.slide_list[self.slide_idx],
                    size=self.root.size, keep_ratio=True,
                    allow_stretch=True,
                    size_hint=(None, None))
            self.root.add_widget(self._slide)

    def play_slide_show(self):
        
        '''
        play_continue_slide_show schedules but does not change self.slide_idx.
        '''

        if self.slide_list:
            self.slide_event = Clock.schedule_interval(self.show_slide,
                               self.slide_speed)

    def pause_slide_show(self):

        '''
        pause_slide_show pauses the scheduled slide_show_event and the slide
        index referencing by integer the slide show.
        '''

        if self.slide_list:
            self.slide_event = Clock.unschedule(self.show_slide)


    def stop_slide_show(self):

        '''
        stop the slide show unschedules the slide show regardless of the
        slide index.
        '''

        if self.slide_list:
            self.slide_event = Clock.unschedule(self.show_slide)
            Clock.schedule_once(self.last_slide, self.slide_speed)
            self.slide_list = []
            self.slide_idx = 0


    def next_slide(self):
        
        '''
        next_slide displays the next slide in self.slide_list.
        '''
        
        if self.slide_list:
            if self._slide is not None:
                self.root.remove_widget(self._slide)
            self.slide_idx += 1
            if self.slide_idx >= len(self.slide_list):
                self.slide_idx = 0
            self._slide = Img(
                    source=self.slide_list[self.slide_idx],
                    size=self.root.size, keep_ratio=True,
                    allow_stretch=True,
                    size_hint=(None, None))
            self.root.add_widget(self._slide)

#-----------------------------> Autobrowse <---------------------------------
    def autobrowse(self):

        '''
        autobrowse automatically advances to the next browse segment via 
        scheduling and uses slide show speed setting.
        '''

        if self.filename_list:
            self.browse_event =  Clock.schedule_interval(self.autobrowse_next,
                               self.slide_speed)

    def autobrowse_next(self, _delay):
        
        '''
        autobrowse_next passes in delay and calls self.next to advance
        to the next browse segment.
        '''

        self.next()

    def stop_autobrowse(self):

        '''
        stop_autobrowse unschedules thread for autobrowse.
        '''

        if self.browse_event:
            self.browse_event = Clock.unschedule(self.autobrowse_next)

# -----------------------------> End Autobrowse <-----------------------------
# -----------------------------> End Slide Shows <----------------------------

# ------------------------------> Photo Information <-------------------------

    def get_pic_info(self, _photo):

        '''
        get_pic_information gets information from the selected photo using the
        Python library pillow Image class.
        '''

        _img = Image.open(_photo)
        _filename = _img.filename
        _mode = str(_img.mode)
        _size = str(_img.size)
        _width = str(_img.width)
        _height = str(_img.height)
        _info = str(_img.info)
        _img.close()
        self._photo_info = _filename+'  '+_mode+' '+_size+_width+_height+_info

# -------------------------------> End Photo Information <--------------------

# -------------------------> App Shutdown and Save State <--------------------

    def kpicture_stop(self):

        '''
        kpicture_stop gracefully shutsdown KPicture and saves the state of the
        application.
        '''

        self.root_window.unregister_event_manager(self.hover_manager)
        self.save_cache(self.cache_list)
        self.save_dir(self._directories)
        self.save_curdir(self.curdir)
        self.config.write()
        self.save_settings_config_file(self.settingsfile)
        self.save_settings_list(self._settings)
        self.stop()

    def clear_dir(self):

        '''
        clear_dir clears the App attibute self._directories to empty.
        '''

        self.clear_pics()
        self.filename_list = []
        self.img_idx = 0
        self._x = 0
        self._y = 0
        self._directories = []
        self._directories_idx = 0
        self.save_dir(self._directories)
        self.curdir = '/'
        self.save_curdir(self.curdir)

    def clear_all(self):
        
        '''
        clear_all clears state of kpicture completely.
        '''
        
        self.clear_dir()
        self.clear_cache()
        self._settings = []
        self.load_settings('kpicture.ini')
    
    def save_dir(self, _dir):

        '''
        save_dir saves the directories of pictures in variable
        self._directories to dir.json.
        '''

        self._state_json.put('dir', pdirectories=_dir)

    def save_curdir(self, _current_dir):

        '''
        save_curdir saves the most recent viewed directory for reopening
        when user reopens KPicture.
        '''

        self._state_json.put('lastdir', curdir=_current_dir)

# ---------------------> End App Shutdown and Save State <--------------------
if __name__ == '__main__':
    KPictureApp().run()
